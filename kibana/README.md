# Kibana Values Readme

This readme file includes some values for Kibana that can be customized according to your requirements.

## Extra Envs

The `extraEnvs` value is used to specify environment variables for Kibana. By default, two environment variables are set:

1. `NODE_OPTIONS`: This value is set to `--max-old-space-size=1800`, which specifies the maximum size of the Node.js heap in megabytes. You can customize this value as per your memory requirements.

2. `SERVER_PUBLICBASEURL`: This value is set to `https://kibana.your-org.com`. This URL is used as the base URL for the Kibana server. You must customize this value to match the URL of your Kibana server.

