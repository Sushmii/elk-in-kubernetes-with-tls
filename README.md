# Elastic Stack Kubernetes Deployment

This repository contains a bash script to deploy the Elastic Stack on a Kubernetes cluster.

## Prerequisites

- Kubernetes cluster
- Helm

## Usage

1. Clone the repository
2. Navigate to the repository directory
3. Update the configuration values in the `values.yaml` files for each service
4. The script will ask for Namespace where you want to install ELK stack creates that NS and install it.

### Versions Used

#### Elasticsearch

- `elasticsearch/values.yaml`
  - `imageTag`: 8.5.1

#### Kibana

- `kibana/values.yaml`
  - `imageTag`: 8.5.1

#### Logstash

- `logstash/values.yaml`
  - `imageTag`: 8.5.1

#### Filebeat

- `filebeat/values.yaml`
  - `imageTag`: 7.17.3


### Sample Logstash Configuration

A sample Logstash configuration file is included in the `logstash/pipeline` directory that parses logs from a PHP-FPM application with Nginx as the web server. Customize the configuration according to your requirements.


### Note

The script prompts the user to select the services to install and the namespace to install the services in.

