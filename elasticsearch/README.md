# Elastic Search Values Readme

This readme file includes some values for Elastic Search that can be customized according to your requirements. 

## VolumeClaimTemplate

The `volumeClaimTemplate` value is used to specify the storage options for Elastic Search. By default, the value is set to use a storage of 300Gi with access modes of "ReadWriteOnce". You can adjust this value as per your storage requirements.

## ES Config

The `esConfig` value is used to configure Elastic Search. By default, the `elasticsearch.yml` file is set to `xpack.security.enabled: false`. You can customize this value as per your security requirements.

## Node Selector

The `nodeSelector` value is used to specify which nodes the Elastic Search deployment will be scheduled on. By default, it is set to an empty object, meaning that the Elastic Search deployment can be scheduled on any node.

## ES Java Options

The `esJavaOpts` value is used to configure the Java Virtual Machine (JVM) options for Elastic Search. By default, it is set to use 12 gigabytes of RAM for both the minimum (`Xms`) and maximum (`Xmx`) heap sizes. You can customize this value as per your memory requirements.

## Tolerations

The `tolerations` value is used to specify any taints that are tolerable for the Elastic Search deployment. By default, it is set to tolerate any taints with a key that is equal to an empty string and a value that is equal to "true", with an effect of "NoSchedule". You can adjust this value as per your taint toleration requirements.

## Resources

The `resources` value is used to specify the resource requests and limits for Elastic Search. By default, it is set to request 2 CPU units and 12 gigabytes of memory, with limits of 4 CPU units and 16 gigabytes of memory. You can customize this value as per your resource requirements.
