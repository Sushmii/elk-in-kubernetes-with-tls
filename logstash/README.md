# Logstash Values Readme

This readme file includes some values for Logstash that can be customized according to your requirements.

## Sample Parsing for PHP-FPM Application with Nginx as Webserver

The following is a sample parsing configuration for a PHP-FPM application with Nginx as the webserver. You can use this as a starting point and customize it according to your specific application requirements.
